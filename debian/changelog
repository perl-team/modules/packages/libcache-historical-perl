libcache-historical-perl (0.05-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 10 Dec 2022 15:42:06 +0000

libcache-historical-perl (0.05-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 12:28:27 +0100

libcache-historical-perl (0.05-2) unstable; urgency=medium

  * Team upload

  [ Mats Erik Andersson ]
  * Use a newer DEP-5 standard.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Mats Erik Andersson from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Convert d/copyright to copyright-format 1.0

 -- Florian Schlichting <fsfs@debian.org>  Thu, 21 Jun 2018 23:04:50 +0200

libcache-historical-perl (0.05-1) unstable; urgency=low

  * New upstream release.
  * Remove patches, applied upstream.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 27 Apr 2011 20:45:14 +0200

libcache-historical-perl (0.03-2) unstable; urgency=low

  * Add patch by Niko Tyni to make values() really sort as documented
    (closes: #615882).
  * Add /me to Uploaders.
  * debian/copyright: update license stanzas (point to -GPL-1).
  * Use debhelper 8.
  * Set Standards-Version to 3.9.2 (no further changes).
  * Remove versions from (build) dependencies that are already satisfied in
    oldstable.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Apr 2011 20:15:42 +0200

libcache-historical-perl (0.03-1) unstable; urgency=low

  * Initial release. (Closes: #566280)

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 23 Feb 2010 18:49:27 +0100
